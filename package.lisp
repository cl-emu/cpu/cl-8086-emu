;;;; package.lisp

(defpackage #:cl-8086-emu
  (:use #:cl)
  (:export #:address-calc #:cpu #:make-cpu
	   #:cpu-memory-get-byte #:cpu-memory-set-byte
	   #:cpu-memory-get-word #:cpu-memory-set-word
	   #:cpu-io-get-byte #:cpu-io-set-byte
	   #:cpu-io-get-word #:cpu-io-set-word))
