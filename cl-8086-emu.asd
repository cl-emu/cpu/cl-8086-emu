;;;; cl-8086-emu.asd

(asdf:defsystem #:cl-8086-emu
  :description "An Intel 8086 emulator"
  :author "Melissa Goad <alegend45@gmail.com>"
  :license  "GPLv3+"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "cl-8086-emu")))
