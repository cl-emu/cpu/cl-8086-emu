;;;; cl-8086-emu.lisp

(in-package #:cl-8086-emu)

(declaim (optimize (speed 3) (safety 0)))

(declaim (ftype (function
		 ((unsigned-byte 16) (unsigned-byte 16))
		 (unsigned-byte 20))
		address-calc))

(defun address-calc (segment offset)
  (logand (+ offset (* segment 16)) #xfffff))

(defstruct cpu
  "An Intel 8086 cpu"
  (registers
   (make-array 8 :element-type '(unsigned-byte 16)
	       :initial-element 0)
   :type (simple-array (unsigned-byte 16) (8)))
  (segments
   (make-array 4 :element-type '(unsigned-byte 16)
	       :initial-contents '(0 #xffff 0 0))
   :type (simple-array (unsigned-byte 16) (4)))
  (flags #xf002 :type (unsigned-byte 16))
  (ip 0 :type (unsigned-byte 16))
  (default-segment 3 :type (unsigned-byte 2))
  (current-segment 3 :type (unsigned-byte 2))
  (memory-get-byte
   (lambda (address) (declare (ignore address)) 0)
   :type (function ((unsigned-byte 20)) (unsigned-byte 8)))
  (memory-get-word
   (lambda (address) (declare (ignore address)) 0)
   :type (function ((unsigned-byte 20)) (unsigned-byte 16)))
  (memory-set-byte
   (lambda (address value) (declare (ignore address value)) 0)
   :type (function ((unsigned-byte 20) (unsigned-byte 8)) (unsigned-byte 8)))
  (memory-set-word
   (lambda (address value) (declare (ignore address value)) 0)
   :type (function ((unsigned-byte 20) (unsigned-byte 16)) (unsigned-byte 16)))
  (io-get-byte
   (lambda (address) (declare (ignore address)) 0)
   :type (function ((unsigned-byte 16)) (unsigned-byte 8)))
  (io-get-word
   (lambda (address) (declare (ignore address)) 0)
   :type (function ((unsigned-byte 16)) (unsigned-byte 16)))
  (io-set-byte
   (lambda (address value) (declare (ignore address value)) 0)
   :type (function ((unsigned-byte 16) (unsigned-byte 8)) (unsigned-byte 8)))
  (io-set-word
   (lambda (address value) (declare (ignore address value)) 0)
   :type (function ((unsigned-byte 16) (unsigned-byte 16)) (unsigned-byte 16))))

(defconstant +byte-register-to-word+ '(:al (:ax nil) :ah (:ax t) :bl (:bx nil) :bh (:bx t) :cl (:cx nil) :ch (:cx t) :dl (:dx nil) :dh (:dx t)) "Mapping from byte registers to word registers")
(defconstant +bits-to-register+ '(:ax :cx :dx :bx :sp :bp :si :di) "Mapping from index to word register")
(defconstant +bits-to-byte-register+ '(:al :cl :dl :bl :ah :ch :dh :bh) "Mapping from index to byte register")
(defconstant +bits-to-segment+ '(:es :cs :ss :ds) "Mapping from index to segment register")

(declaim (ftype (function ((unsigned-byte 3)) (keyword))
		bits->word-reg))
(defun bits->word-reg (bits)
  "Map a bit pattern to a word register."
  (aref +bits-to-register+ bits))

(declaim (ftype (function ((unsigned-byte 3)) (keyword))
		bits->byte-reg))
(defun bits->byte-reg (bits)
  "Map a bit pattern to a byte register."
  (aref +bits-to-byte-register+ bits))

(declaim (ftype (function ((unsigned-byte 3)) (keyword))
		bits->segment))
(defun bits->segment (bits)
  "Map a bit pattern to a segment register."
  (aref +bits-to-segment+ (logand bits 3)))
